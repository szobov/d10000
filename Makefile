DIR = 'wildmagic_bot/'
VERSION = '0.9'
IMAGE_NAME = 'wildmagic_bot:$(VERSION)'
ARCH_NAME = 'wildmagic_bot_$(VERSION).tar'

check:
	pycodestyle $(DIR) && pep257 $(DIR) && mypy $(DIR)

build_and_upload:
	rm -r dist/ && \
	python setup.py sdist && \
	docker build -t $(IMAGE_NAME) . && \
	docker save -o $(ARCH_NAME) $(IMAGE_NAME) && \
  docker rmi $(IMAGE_NAME) && \
	scp $(ARCH_NAME) digitalocean:/tmp/ && \
	rm $(ARCH_NAME) && \
  ssh digitalocean 'docker load -i /tmp/$(ARCH_NAME)' && \
	ssh digitalocean 'rm /tmp/$(ARCH_NAME)' && \
  ssh digitalocean 'docker stop wildmagic_bot' && \
  ssh digitalocean 'docker rm wildmagic_bot' && \
	ssh digitalocean 'docker run -d --name wildmagic_bot --restart=always $(IMAGE_NAME)'
