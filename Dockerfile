FROM frolvlad/alpine-python3

ADD settings/ /settings/
ADD dist/ /dist/
RUN pip install /dist/* && rm -rf /dist/ /root/.cache/

ENTRYPOINT ["wildmagic_bot_run", "--config", "/settings/settings.yaml"]
