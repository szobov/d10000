from setuptools import setup, find_packages

NAME = 'wildmagic_bot'
VERSION = 0.9

REQUIRED = [
    'pyyaml',
    'google-auth',
    'requests',
    'aiotg',
    'click'
]

setup(
    name=NAME,
    version=VERSION,
    entry_points={
        'console_scripts': [
            'wildmagic_bot_run = wildmagic_bot:main',
        ]
    },
    packages=find_packages(exclude=('tests',)),
    install_requires=REQUIRED,
    extras_require={
        'dev': [
            'pytest',
        ]
    }
)
