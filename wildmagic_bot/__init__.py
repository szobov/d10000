"""Telegram bot thet gives you random wildmagic from d10000 wildmagic."""
from asyncio import get_event_loop
from json import dumps
from logging import basicConfig, getLogger
from random import randint
from typing import Any, Dict, List

import click
from aiotg import Bot, Chat
from google.auth.transport.requests import AuthorizedSession
from google.oauth2 import service_account
from yaml import load


class MagicFetcher:
    """Bot class."""

    _MAGIC_COUNT_MAX = 9999
    _MAGIC_COUNT_MIN = 1
    _URL = (
        'https://sheets.googleapis.com/v4/spreadsheets/'
        '{table}/values/{sheet}!A{{number}}')
    _BUTTON_TEXT = 'I want random!'
    _BOT_NAME = 'wildmagic_bot'
    _HELP_TEXT = (
        'This bot interested for you only if you play in '
        '[Dungeon and Drugons](http://dnd.wizards.com/).'
        '\n\n'
        'If you tap on button or write the number of desired magic, '
        'it returns you description of this magic.'
        '\n\n'
        'Bot gets description for local copy of this '
        '[list](https://goo.gl/MtiFzm).'
        '\n\n'
        '*Roll and enjoy!*')

    def __init__(self,
                 bot_token: str,
                 google_credentials: Dict[str, Any],
                 table_settings: Dict[str, str]) -> None:
        """
        Bot constructor.

        Prepare telegram bot and oauth2 session for google-api.

        :param bot_token: str, contains telegram bot token. More information
                          here: `https://core.telegram.org/bots/faq`.
        :param google_credentials: dict, contains path to json secret-key file
                                   and list of google api scopes.
        :param table_settings: dict, contains name of google sheet table
                               and worksheet
        """
        self._bot = self._prepate_bot(bot_token)
        self._session = AuthorizedSession(
            self._get_google_credential(google_credentials))
        self._url = self._URL.format(
            table=table_settings['table'], sheet=table_settings['sheet'])

        self._loop = get_event_loop()
        self._log = getLogger(self._BOT_NAME)

    def _prepate_bot(self, bot_token: str) -> Bot:
        """
        Prepare telegram bot and its commands.

        :param bot_token: str, telegram bot token.

        :return: `aiotg.bot.Bot`, prepared bot with commands.
        """
        bot = Bot(bot_token)
        bot.add_command(r'/start', self._start)
        bot.add_command(r'/roll', self._process_roll)
        bot.add_command(r'/help', self._help)
        bot.default(self._process_text)
        return bot

    def _get_google_credential(self, settings: Dict[str, Any]) -> Any:
        """
        Prepare google credentials for oauth2 session.

        :param settings: dict, contains path to json secret-key file
                                   and list of google api scopes.

        :return: `google.oauth2.service_account.Credentials`, prepared creds.
        """
        return (service_account
                .Credentials
                .from_service_account_file(settings['file_path'])
                .with_scopes(settings['scopes']))

    async def _get_magic(self, number: int) -> str:
        """
        Get rundom wild magic from google sheet table.

        :param number: `int`, number of wild magic.

        :return: `str` with magic's description.
        """
        result = await self._loop.run_in_executor(None, lambda: (
            self._session.get(self._url.format(number=number)).json()
        ))  # type: Dict[str, List[List[str]]]
        return result['values'][0][0]

    async def _send_text(self, chat: Chat, text: str,
                         *args: Any, **kwargs: Any) -> None:
        """Send markdown text to user."""
        await chat.send_text(
            text,
            parse_mode='Markdown',
            disable_notification=True,
            *args, **kwargs)

    async def _send_answer(self, chat: Chat, number: int = None) -> None:
        """
        Send user wild magic decription.

        :param chat: `aiotg.chat.Chat`, targeted user.
        :param number: optional int, number of needed wild magic or None, if
                       user needs description of random wild magic.
        """
        if number is None:
            number = randint(self._MAGIC_COUNT_MIN, self._MAGIC_COUNT_MAX)
        text = await self._get_magic(number)
        await self._send_text(chat, f'_{number}._\n*{text}.*')

    async def _process_roll(self, chat: Chat, _: Any) -> None:
        """Process /roll command and give to user description of wild magic."""
        self._log.debug('Processing command /roll')
        await self._send_answer(chat)

    async def _process_text(self, chat: Chat, message: Dict[str, Any]) -> None:
        """Process text and give to user description of wild magic."""
        self._log.debug(f'Processing message: "{message}". From chat "{chat}"')
        text: str = message['text'].strip()
        if text == self._BUTTON_TEXT:
            number = None
        else:
            try:
                number = int(text)
            except ValueError:
                return await self._send_text(
                    chat, 'Please, type the number or press the roll')
            if (
                    (number < self._MAGIC_COUNT_MIN) or
                    (number > self._MAGIC_COUNT_MAX)
            ):
                return await self._send_text(
                    chat,
                    'Please, type the number in range from '
                    f'{self._MAGIC_COUNT_MIN} to {self._MAGIC_COUNT_MAX}')
        await self._send_answer(chat, number)

    async def _start(self, chat: Chat, _: Any) -> None:
        """Process /start command and send to user answer with keyboard."""
        await self._send_text(
            chat,
            self._HELP_TEXT,
            reply_markup=self._get_keyboard()
        )

    async def _help(self, chat: Chat, _: Any) -> None:
        """Process /help and send to user bot description."""
        await self._send_text(chat, self._HELP_TEXT)

    def _get_keyboard(self) -> str:
        """Get json with keyboard markup `https://goo.gl/4gVDQh`."""
        return dumps({
            'keyboard': [[{'text': self._BUTTON_TEXT}]],
            'one_time_keyboard': False,
            'resize_keyboard': True
        })

    def run(self) -> None:
        """Run bot in until catch Ctrl+C."""
        try:
            self._loop.run_until_complete(self._bot.loop())
        except KeyboardInterrupt:
            self._bot.stop()


@click.command()
@click.option('--verbose', default=False, help='Enable debug logging.')
@click.option('--config', default='settings.yaml', type=click.Path(),
              help='Path to config file.')
def main(verbose: bool, config: str) -> None:
    """Read config and start bot."""
    if verbose:
        basicConfig(level='DEBUG')

    with open(config) as settings_file:
        settings: Dict[str, Any] = load(settings_file)

    magic_fetcher = MagicFetcher(
        settings['telegram_token'],
        settings['google_credentials'],
        settings['sheet_settings'])
    magic_fetcher.run()


if __name__ == '__main__':
    main()
