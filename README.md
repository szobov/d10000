**Wildmagic bot**

This [telegram-bot](https://core.telegram.org/bots/) was created for my friends to help them play in [Dungeon and Drugons](http://dnd.wizards.com/).

Bot does only one function: it can give you description wildmagic by number, rundom or now.
It takes descriptions from the copy of this [document](https://goo.gl/MtiFzm).
The copy lives in my [googlesheets](https://www.google.com/sheets/about/).

My instance of this bot available [here](https://t.me/d10000_bot).
